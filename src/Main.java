import org.w3c.dom.ls.LSOutput;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args){

        int year=2021;
        LocalDate startDate = LocalDate.of(year,1,1);
        LocalDate endDate = LocalDate.of(year,12,31);

        List<LocalDate> friday13InYear = getNumberOfFriday13(startDate, endDate);

        System.out.println("Number of occurrences of Friday 13th in a year " + year + " is " + friday13InYear.size());
        System.out.println("The dates:");
        friday13InYear.forEach(day -> System.out.println(day));


    }

    public static List<LocalDate> getNumberOfFriday13( LocalDate startDate, LocalDate endDate) {

           List<LocalDate> friday13InYear = startDate.datesUntil(endDate)
                   .filter(day -> day.getDayOfMonth() == 13)
                  .filter(day -> day.getDayOfWeek().equals(DayOfWeek.FRIDAY))
                .collect(Collectors.toList());

           return friday13InYear;
    }
}
